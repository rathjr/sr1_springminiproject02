package com.example.sr01_springminiproject.Repository;

import com.example.sr01_springminiproject.Model.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserRepo {

    @Insert("insert into users(fullname,password,username) values(#{fullname},#{password},#{username})")
    @Options(useGeneratedKeys = true,keyProperty = "id")
    boolean registerUser(User user);

    @Select("select * from users where username=#{username}")
    @Options(useGeneratedKeys = true,keyProperty = "id")
    User findUserbyUsername(String username);

    @Select("select * from users where id=#{id}")
    User findUserByID(int id);


}
