package com.example.sr01_springminiproject.Config;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

@MapperScan("com.example.sr01_springminiproject.Repository;")
public class mybatisConfig {
    @Autowired
    DataSource dataSource;

    @Bean
    public DataSourceTransactionManager dataSourceTransactionManager() {

        return new DataSourceTransactionManager(dataSource);

    }
    @Bean

    public SqlSessionFactoryBean sqlSessionFactoryBean() {

        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();

        sqlSessionFactoryBean.setDataSource(dataSource);

        return sqlSessionFactoryBean;

    }
}
