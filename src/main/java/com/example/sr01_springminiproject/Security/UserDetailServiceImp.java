package com.example.sr01_springminiproject.Security;

import com.example.sr01_springminiproject.Model.User;
import com.example.sr01_springminiproject.Repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailServiceImp implements UserDetailsService{
    @Autowired
    UserRepo userRepo;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user= userRepo.findUserbyUsername(username);
        System.out.println("user = "+user);
        if(user==null){
            return null;
        }else {
            UserDetailImp userDetailImp = new UserDetailImp();
            userDetailImp.setUsername(user.getUsername());
            userDetailImp.setPassword(user.getPassword());
            return userDetailImp;
        }
    }
}
