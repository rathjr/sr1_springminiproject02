package com.example.sr01_springminiproject.Security.jwt;

import com.example.sr01_springminiproject.Security.UserDetailImp;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

//generateToken , verify.....
@Component
public class Jwtutils {

    @Value("${app.jwt.secret}")
    public String secret;
    @Value("${app.jwt.expiration}")
    public long expiration;

    public String generateToken(Authentication authentication){
//        get data user
        UserDetailImp userDetailImp=(UserDetailImp) authentication.getPrincipal();
//        generate token
        return Jwts
                .builder()
                .setSubject(userDetailImp.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(new Date(new Date().getTime()+expiration))
                .signWith(SignatureAlgorithm.HS512,secret)
                .compact();
    }
//    done generate toek

//    declare method is have another exception arise
//    get username
        public String getUsername(String token){
        if(verifyJwtToken(token)) {
            return Jwts
                    .parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody()
                    .getSubject();
        }
        return null;
        }
        public boolean verifyJwtToken(String token){
        try {
            Jwts
                    .parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token);
            return true;
        }catch (SignatureException e) {
            System.out.println("invalid JWT signature" + e.getMessage());
        }catch (MalformedJwtException e){
                System.out.println("invalid JWT Token" + e.getMessage());
        }catch (ExpiredJwtException e){
            System.out.println("invalid JWT Expire" + e.getMessage());
        }catch (UnsupportedJwtException e){
            System.out.println("Unsupport Jwt token" + e.getMessage());
        }catch (IllegalArgumentException e){
            System.out.println("JWT claim String is empty" + e.getMessage());
        }catch (Exception e){
            System.out.println("Exception" + e.getMessage());
        }
        return false;
        }
}
