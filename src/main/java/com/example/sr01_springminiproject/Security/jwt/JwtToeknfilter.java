package com.example.sr01_springminiproject.Security.jwt;


import com.example.sr01_springminiproject.Security.UserDetailImp;
import com.example.sr01_springminiproject.Security.UserDetailServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//request mdong filter mdong
//vea verify knea
public class JwtToeknfilter extends OncePerRequestFilter {

    @Autowired
    private Jwtutils jwtutils;

    @Autowired
    private UserDetailServiceImp userDetailServiceImp;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        try{
            String jwt= parseJwt(request);
            if(jwt!=null && jwtutils.verifyJwtToken(jwt)){
                String username=jwtutils.getUsername(jwt);
                UserDetailImp userDetailImp=(UserDetailImp) userDetailServiceImp.loadUserByUsername(username);
                UsernamePasswordAuthenticationToken authentication =
                        new UsernamePasswordAuthenticationToken(userDetailImp,null ,userDetailImp.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
            filterChain.doFilter(request,response);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
    private String parseJwt(HttpServletRequest request){
        String header = request.getHeader("Authorization");
        String prefix = "Bearer ";
        if(StringUtils.hasText(header) && header.startsWith(prefix)){
            return header.substring(prefix.length());
        }
        return null;
    }
}
