package com.example.sr01_springminiproject.Controller.Rest;

import com.example.sr01_springminiproject.Model.User;
import com.example.sr01_springminiproject.Payload.AuthRequest.LoginRequest;
import com.example.sr01_springminiproject.Payload.AuthRequest.RegisterRequest;
import com.example.sr01_springminiproject.Payload.Dto.JwtResponse;
import com.example.sr01_springminiproject.Payload.Dto.RegisterResponse;
import com.example.sr01_springminiproject.Payload.Mapper.UserMapper;
import com.example.sr01_springminiproject.Payload.ResponseEntity;
import com.example.sr01_springminiproject.Repository.UserRepo;
import com.example.sr01_springminiproject.Security.jwt.Jwtutils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
public class AuthRestController {
    @Autowired
    private Jwtutils jwtutils;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private UserRepo userRepo;
    @Autowired
    UserMapper userMapper;
// take username password tv authenticate

    @PostMapping("/login")
    public ResponseEntity<JwtResponse> loginUser(@RequestBody LoginRequest loginRequest){
        Authentication authentication=authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(),
                        loginRequest.getPassword()
                ));
//        generate token
        String jwt=jwtutils.generateToken(authentication);
        JwtResponse jwtResponse=new JwtResponse();
        User user= userRepo.findUserbyUsername(loginRequest.getUsername());
        System.out.println("testid"+user.getId());
        jwtResponse.setId(user.getId());
        jwtResponse.setUsername(loginRequest.getUsername());
        jwtResponse.setToken(jwt);
            return ResponseEntity.<JwtResponse>ok().setPayload(jwtResponse);
        }

    @PostMapping("/register")
    public ResponseEntity<RegisterResponse> registerUser(@RequestBody RegisterRequest registerRequest){
        registerRequest.setPassword(passwordEncoder.encode(registerRequest.getPassword()));
        User user=userMapper.registerRequestToUser(registerRequest);
        boolean is_create=userRepo.registerUser(user);
        System.out.println("password"+registerRequest.getPassword());
        System.out.println("after encode" + passwordEncoder.encode(registerRequest.getPassword()));
        user.setPassword(passwordEncoder.encode(registerRequest.getPassword()));
        if(is_create){
            RegisterResponse response=userMapper.usertoRegisterResponse(user);
            return ResponseEntity.<RegisterResponse>ok().setPayload(response);
        }
        else
            return ResponseEntity.<RegisterResponse>badRequest().setErrors("Check your Data again");
    }

}
