package com.example.sr01_springminiproject.Controller.Rest;

import com.example.sr01_springminiproject.Model.User;
import com.example.sr01_springminiproject.Payload.Dto.JwtResponse;
import com.example.sr01_springminiproject.Payload.Mapper.UserMapper;
import com.example.sr01_springminiproject.Payload.ResponseEntity;
import com.example.sr01_springminiproject.Repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class test {
    @Autowired
    UserRepo userRepo;
    @Autowired
    UserMapper userMapper;
    @GetMapping("/{id}")
    public ResponseEntity<JwtResponse> finduserByID(@PathVariable int id){
        User user=userRepo.findUserByID(id);
        if(user==null){
            return ResponseEntity
                    .<JwtResponse>notFound()
                    .setErrors("your id "+id + "is not found !");
        }
        JwtResponse jwtdto=userMapper.usertojwtResponse(user);
        return ResponseEntity.<JwtResponse>ok().setPayload(jwtdto);
    }

}
