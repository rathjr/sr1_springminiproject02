package com.example.sr01_springminiproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Sr01SpringminiprojectApplication {

    public static void main(String[] args) {
        SpringApplication.run(Sr01SpringminiprojectApplication.class, args);
    }

}
