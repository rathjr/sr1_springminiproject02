package com.example.sr01_springminiproject.Model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class User {
    private int id;
    private String username;
    private String password;
    private String fullname;
    private boolean is_account_closed;
    private String profile;
    List<Role> roleList;
}
