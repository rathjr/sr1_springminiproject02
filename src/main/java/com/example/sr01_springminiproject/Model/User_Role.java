package com.example.sr01_springminiproject.Model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class User_Role {
    private int User_id;
    private int Role_id;
}
