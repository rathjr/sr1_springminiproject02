package com.example.sr01_springminiproject.Model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class Role {
    private int id;
    private String name;
}
