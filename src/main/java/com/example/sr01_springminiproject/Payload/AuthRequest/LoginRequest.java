package com.example.sr01_springminiproject.Payload.AuthRequest;

import lombok.Getter;
import lombok.Setter;
@Setter
@Getter
public class LoginRequest {
        private String username;
        private String password;
}
