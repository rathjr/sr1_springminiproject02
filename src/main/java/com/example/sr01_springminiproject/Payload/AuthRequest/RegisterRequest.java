package com.example.sr01_springminiproject.Payload.AuthRequest;

import com.example.sr01_springminiproject.Model.Role;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
@Setter
@Getter
@ToString
public class RegisterRequest {
    private String fullname;
    private String password;
    private List<Role> roleList;
    private String username;
}
