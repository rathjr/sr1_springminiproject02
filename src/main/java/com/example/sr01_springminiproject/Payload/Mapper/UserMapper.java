package com.example.sr01_springminiproject.Payload.Mapper;

import com.example.sr01_springminiproject.Model.User;
import com.example.sr01_springminiproject.Payload.AuthRequest.RegisterRequest;
import com.example.sr01_springminiproject.Payload.Dto.JwtResponse;
import com.example.sr01_springminiproject.Payload.Dto.RegisterResponse;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {

     JwtResponse usertojwtResponse(User user);
     User jwtResponseToUser(JwtResponse jwtResponse);

     RegisterResponse usertoRegisterResponse(User user);
     User registerRequestToUser(RegisterRequest registerRequest);
}
