package com.example.sr01_springminiproject.Payload.Dto;

import com.example.sr01_springminiproject.Model.Role;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
@Setter
@Getter
@ToString
public class RegisterResponse {
    private int id;
    private String fullname;
    private String username;
    private String password;
    private String profile;
    private List<Role> roleList;
}
