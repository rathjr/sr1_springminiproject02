package com.example.sr01_springminiproject.Payload.Dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class JwtResponse {
    private int id;
    private String username;
    private String token;
}
