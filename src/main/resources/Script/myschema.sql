create table users
(
    id bigint auto_increment,
    username varchar(100) not null,
    password   varchar(100) not null,
    fullname   varchar(30),
    profile varchar(30),
    is_account_closed BOOLEAN
);
create table role
(
    id    number primary key,
    name varchar(100) not null
);
create table users_role
(
    user_id number,
    role_id number
);
Alter table users_role
    Add Constraint  Fk_users_role FOREIGN KEY (user_id) REFERENCES role(id);

create table posts
(
-- input field here
);
create table comments
(
-- input field here
);
create table users_like_post
(
-- input field here
);